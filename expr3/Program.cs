﻿using System;

namespace expr4
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			int h;

			if (!int.TryParse(Console.ReadLine(), out h) || h < 0)
			{
				Console.Error.WriteLine("Bad integer for hours!");
				return;
			}

			h %= 12;	//переводим в часы на стрелочных часах (от 0 до 12)
			if (h > 6)
				h = 12 - h;

			Console.WriteLine(30 * h);
			Console.ReadKey();
		}
	}
}
