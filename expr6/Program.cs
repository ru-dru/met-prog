﻿using System;
using System.Collections.Generic;

namespace expr6
{
    class Point
    {
        private double x, y;

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        public double Y
        {
            get { return y; }
            set { y = value; }
        }
    }

    class Line
    {
        private Point p1, p2;

        //коэффициенты ур-ния Ax+By+C=0
        private List<double> СoefficientsOfLineEquation()
        {
            List<double> result = new List<double>();
            result.Add(p1.Y - p2.Y);
            result.Add(p2.X - p1.X);
            result.Add(p1.X*p2.Y + p2.X*p1.Y);

            return result;
        }
        
        //Строим линию по 2ум точкам
        public Line(Point p1, Point p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        //Расстояние от точки p до данной прямой
        public double Dest(Point p)
        {
            var coefficients = СoefficientsOfLineEquation();
            var A = coefficients[0];
            var B = coefficients[1];
            var C = coefficients[2];

            return Math.Abs(A * p.X + B * p.Y + C) / Math.Sqrt(A * A + B * B);
        }
    }
    
    internal class Program
    {
        /*
         * Читаем 6 числа (через пробел)
         * Первые 2 числа - первая точка прямой
         * Вторые 2 числа - вторая точка прямой
         * Третьи 2 числа - точка, от которой ищем расстояние до прямой
         */
        public static void Main(string[] args)
        {
            double x1, y1, x2, y2, x3, y3;
            var numbers = Console.ReadLine().Split(' ');
            if (numbers.Length < 6)
            {
                Console.WriteLine("not enought numbers - should be 6 them");
                return;
            }
            if (!double.TryParse(numbers[0], out x1))
            {
                Console.WriteLine("Bad double");
                return;
            }
            if (!double.TryParse(numbers[1], out y1))
            {
                Console.WriteLine("Bad double");
                return;
            }
            if (!double.TryParse(numbers[2], out x2))
            {
                Console.WriteLine("Bad double");
                return;
            }
            if (!double.TryParse(numbers[3], out y2))
            {
                Console.WriteLine("Bad double");
                return;
            }
            if (!double.TryParse(numbers[4], out x3))
            {
                Console.WriteLine("Bad double");
                return;
            }
            if (!double.TryParse(numbers[5], out y3))
            {
                Console.WriteLine("Bad double");
                return;
            }
            
            Console.WriteLine(new Line(new Point(x1, y1), new Point(x2, y2)).Dest(new Point(x3, y3)));
        }
    }
}